# Portable JDK 8u112 for Windows x64 #

This is a portable version of JDK 8u112 for Windows x64.

### Why portable JDK? ###

Because installing with Oracle Installer sucks.

### How do I get set up? ###

*  just clone this repository wherever you want:


```
#!cmd

mkdir c:\JDK
cd c:\JDK
git clone https://bitbucket.org/ramazanpolat/jdk-8u112-windows-x64

```



That's it. Now you have installed JDK 8u112.